extends Node

var up = Vector3(0, 1, 0)
var min_distance = 25
var min_height = 20

func _fixed_process(dt):
	var target = get_node("/root/game/world").player

	if target == null:
		return
		
	var tpos = target.get_global_transform().origin
	var pos = tpos + Vector3(0, 0, 1)*min_distance + Vector3(0, 1, 0)*min_height
	
	look_at_from_pos(pos,tpos,up)


func _ready():
	set_fixed_process(true)

