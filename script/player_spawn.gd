extends Node

var world
var player_inst = preload("res://prefab/player.scn")

var points = null

func select_point():
	if points == null:
		return Vector3(0, 0, 0)

	var idx = randi() % points.size()

	return points[idx].get_translation()


func spawn():
	var obj = player_inst.instance()
	var pos = select_point()
	obj.set_translation(pos)
	
	world.add_child(obj)
	

func _process(dt):
	if world.player == null:
	
		spawn()
	
	elif world.player.is_ded():
	
		world.player.get_parent().remove_child(world.player)
		world.player = null
		spawn()


func _ready():
	points = get_tree().get_nodes_in_group("player_spawn")
	world = get_node("/root/game/world")
	set_process(true)