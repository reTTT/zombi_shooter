extends Node

export var need_paused = false

var active_dlg = null
var dlg_stack = []

var dialogs = {}

func show_dlg(dlg_id):
	if active_dlg != null:
		dlg_stack.push_back(active_dlg)
		active_dlg = null

	var keys = dialogs.keys()
	for k in keys:
		var dlg = dialogs[k]

		if dlg.get_name() == dlg_id:
			dlg.show()
			active_dlg = dlg
			break

	if active_dlg != null &&  !get_tree().is_paused() && need_paused:
		get_tree().set_pause(true)


func hide_dlg():
	if active_dlg == null:
		return

	active_dlg.hide()
	dlg_stack = []

	if get_tree().is_paused() && need_paused:
		get_tree().set_pause(false)


func hide_and_back():
	if active_dlg == null:
		return

	active_dlg.hide()
	active_dlg = null

	if dlg_stack.empty():
		if get_tree().is_paused() && need_paused:
			get_tree().set_pause(false)
		return

	active_dlg = dlg_stack[dlg_stack.size()-1]
	dlg_stack.remove(dlg_stack.size()-1)

	active_dlg.show()


func _ready():
	for c in get_children():
		if c extends Control:
			dialogs[c.get_name()] = c
			c.hide()
