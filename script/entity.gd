extends RigidBody

export var health = 20
export var ded_zone_level = -5

var damage = 0
var ray_floor
var ded = false

func _init():
	add_user_signal("on_dead")


func on_dead():
	ded = true
	emit_signal("on_dead")


func is_ded():
	return ded;


func add_damage(d):
	damage += d

	damage = clamp(damage, 0, health)
	if get_health() == 0:
		on_dead()


func get_health():
	return health - damage


func _process(dt):
	var pos = get_translation()
	if pos.y < ded_zone_level:
		on_dead()


func can_move():
	if ray_floor == null:
		return true
	
	return ray_floor.is_colliding()


func _ready():
	ray_floor = get_node("ray_floor")
	damage = 0
	set_process(true)

