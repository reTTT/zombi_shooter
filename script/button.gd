extends Node2D

var pressed = false setget , _get_pressed

func _get_pressed():
	return pressed != 0

func is_inside(pos):
	var t = self.get_global_transform()
	t = t.inverse()
	var p = t.xform(pos)
	var r = self.get_item_rect()
	
	return r.has_point(p)
	

func _input(e):
	if !is_visible():
		return
	
	if e.type == InputEvent.MOUSE_BUTTON:
		if e.pressed:
			if is_inside(e.pos):
				pressed = true
		else:
			pressed = false
	
	elif e.type == InputEvent.SCREEN_TOUCH:
		
		if e.pressed:
			if is_inside(e.pos) && !_get_pressed():
				pressed = e.index
		elif e.index == pressed:
			pressed = 0


func _ready():
	pressed = false
	set_process_input(true)