extends Node

var prefab = preload("res://prefab/enemy.scn")
var cur_inst = 4
var count = 0
var delay = 0
var dead_count = 0

var statistic

var points = null;

func find_free_point():
	if points == null:
		return null

	var idx = randi() % points.size()
	
	var child = points[idx]
	
	var bodies = child.get_overlapping_bodies()
	
	if !bodies.empty():
		return null
	
	return child


func spawn():
	var cpoint = find_free_point()

	if cpoint == null:
		return

	var pos = cpoint.get_translation()

	var obj = prefab.instance()
	obj.set_translation(pos)
	obj.spawner = self
	add_child(obj)
	
	count -= 1


func dead_enemy(enemy):
	dead_count += 1
	
	if dead_count >= cur_inst:
		dead_count = 0
		next_wave()


func next_wave():
	cur_inst *= 2
	count = cur_inst
	
	if statistic:
		statistic.set_wave(statistic.wave + 1)


func _process(dt):
	
	delay -= dt
	
	if count > 0 && delay < 0:
		spawn()
		delay = .5



func _ready():
	points = get_tree().get_nodes_in_group("enemy_spawn")
	statistic = get_node("/root/game/statistic")
	count = cur_inst
	set_process(true)
	pass

