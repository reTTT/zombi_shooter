extends Spatial

var rays = []
var detect = false

var i = 0

func is_detect():
	if detect == false:
		print("false " + str(i))
		i += 1

	return detect


func _fixed_process(dt):
	detect = false

	for r in rays:
		if r.is_colliding():
			detect = true
			break


func _ready():
	for c in get_children():
		if c extends RayCast:
			rays.push_back(c)
	
	set_fixed_process(true)
