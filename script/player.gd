extends "res://script/entity.gd"

export var auto_aim = true
export var aim_dist = 8
export var speed = 5.0
var mpos

var world

var joystick

func get_directin():

	if !can_move():
		return null

	if joystick && joystick.active:
		var jd = joystick.direction
		return Vector3(jd.x, 0, jd.y)

	var m_left = Input.is_action_pressed("move_left");
	var m_right = Input.is_action_pressed("move_right");
	var m_up = Input.is_action_pressed("move_up");
	var m_down = Input.is_action_pressed("move_down");
	
	if !m_left && !m_right && !m_up && !m_down:
		return null

	var d = Vector3(0, 0, 0)
	
	if m_left:
		d.x = -1
	elif m_right:
		d.x = 1
	
	if m_up:
		d.z = -1
	elif m_down:
		d.z = 1
	
	return d


func _fixed_process(dt):
	var dir = get_directin()
	
	if dir == null:
		return

	set_linear_velocity(dir * speed)
	
	var pos = null
	var aim = false
	
	if auto_aim:
		var target = world.find_target(aim_dist)
	
		if target != null:
			pos = target.get_translation()
			pos.y = get_translation().y
			aim = true
	
	if !aim:
		pos = get_translation() + dir * aim_dist

	var tr = get_transform().looking_at(pos, Vector3(0, 1, 0))
	set_transform(tr)


func _ready():
	
	health = 100
	world = get_node("/root/game/world")
	joystick = get_node("/root/game/ui/joystick/stick")
	world.player = self
	
	set_fixed_process(true)
